const base_url = "https://63f442e1de3a0b242b506338.mockapi.io";

// lấy dssv từ api
function laydssvtuapi() {
  batloading();
  axios({ url: `${base_url}/sv`, method: "GET" })
    .then(function (res) {
      var dssv = res.data;
      console.log("dssv: ", dssv);
      renderdssv(dssv);
      tatloading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatloading();
    });
}
laydssvtuapi();
// render dssv ra giao diện
function renderdssv(dssv) {
  var contenhtml = "";
  for (var i = dssv.length - 1; i > 0; i--) {
    var item = dssv[i];
    // console.log("item: ", item);
    var contentr = ` <tr> 
    <td> ${item.ma}</td> 
    <td> ${item.ten}</td> 
    <td> ${item.email}</td> 
    <td> 0 </td> 
    <td> 
    <button 
    onclick="xoasv('${item.ma}')"
    class="btn btn-danger">xóa </button>
    <button 
    onclick="suasv('${item.ma}')"
    class="btn btn-warning">sửa</button>
     </td> 
     
    </tr> `;
    contenhtml += contentr;
  }

  document.getElementById("tbodySinhVien").innerHTML = contenhtml;
}
// xóa sv từ  giao diện
function xoasv(id) {
  batloading();
  axios({ url: `${base_url}/sv/${id}`, method: "DELETE" })
    .then(function (res) {
      laydssvtuapi();
      tatloading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatloading();
    });
}
// thêm sinh viên
function themsv() {
  // lấy thông tin từ form
  batloading();
  var sv = laythongtintuform();
  axios({ url: `${base_url}/sv`, method: "POST", data: sv })
    .then(function (res) {
      laydssvtuapi();
      tatloading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatloading();
    });
}
// sửa sinh viên
function suasv(id) {
  //lấy sv có id truyền vào từ api
  batloading();
  axios({ url: `${base_url}/sv/${id}`, method: "GET" })
    .then(function (res) {
      //show thông tin lên form
      showthongtinlenform(res.data);
      tatloading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatloading();
    });
}
// cập nhật sinh viên
function capnhatsv() {
  var sv = laythongtintuform();
  batloading();
  axios({ url: `${base_url}/sv/${sv.ma}`, method: "PUT", data: sv })
    .then(function (res) {
      laydssvtuapi();
      tatloading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatloading();
    });
}
function reset() {
  document.getElementById("formQLSV").reset;
}
